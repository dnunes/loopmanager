'use strict';

const BaseStep = require('./BaseStep')
;

class BaseCollector extends BaseStep {
  constructor() {
    super();
    this._requester = this.getRequester();
    this.cachedData = {};

    this._cachedData = {};
    this.DEFAULT_CACHE_KEY = '_default';

    this._resetTransactionalData();
  }
  _resetTransactionalData() {
    this._returnData = {};
    this._queueData = {
      'waitingFor': [],
      'errors': []
    };
  }

  //Returns an object that extends the `BaseRequester` class
  getRequester() {
    throw new Error('THIS NEEDS TO BE IMPLEMENTED ON CHILDREN CLASS!');
  }
  //Returns an array of data to be used as IDs for each call to `Requester.check()`
  getLoopOverData() {
    throw new Error('THIS NEEDS TO BE IMPLEMENTED ON CHILDREN CLASS!');
  }
  //Returns the params to be passed to `Requester.check()`
  getCheckParams(curLoopData) { return curLoopData; }
  //Returns the jsonData to be saved in the shared
  treatRequesterReturn(jsonData/*, xtraData*/) { return jsonData; }
  //Returns the key to be used as index for each data collected
  getReturnDataKey(xtraData) { return xtraData; }


  //Local cache
  getCachedData(key) {
    if (!key) { key = this.DEFAULT_CACHE_KEY; }
    //FIXME Implement cache expiration bases on "[key]['time']"
    let cacheInfo = this._cachedData[key];
    if (!cacheInfo) { return false; }

    let now = new Date();
    let cacheTime = cacheInfo['time'];
    console.log('elapsed', now -cacheTime);
    console.log('now', now);
    console.log('now plus6', now +(1000 *60 *6));
    return this._cachedData[key] && this._cachedData[key]['value'];
  }
  setCachedData(value, key) {
    if (!key) { key = this.DEFAULT_CACHE_KEY; }
    this.cachedData[key] = {'time': new Date(), 'value': value};
  }


  //Mainflow
  _run() {
    this._resetTransactionalData();

    let loopOver = this.getLoopOverData();
    if (!loopOver.length) { //no loopable items: just skip.
      return this._checkIfFinished();
    }
    loopOver.forEach((loopItem) => {
      let checkParams = this.getCheckParams(loopItem);
      this._queueData['waitingFor'].push(checkParams);
      this._requester.check(checkParams, this._afterRequester.bind(this));
    });
    return true;
  }
  _afterRequester(jsonData, err, xtraData) { //raw return from requester
    if (this._sharedStateManager.get('_curStepID') !== this.getID()) {
      return false; //aborted!
    }

    let dataIndex = this._queueData['waitingFor'].indexOf(xtraData);
    if (~dataIndex) {
      this._queueData['waitingFor'].splice(dataIndex, 1);
    }

    if (err) {
      let msg = 'Requester returned an error to collector: "'+ err +'"';
      return this._err(new Error(msg), xtraData);
    }
    if (!jsonData) { return this._empty(xtraData); }

    let treatedData = this.treatRequesterReturn(jsonData, xtraData);
    if (!treatedData) {
      let msg = 'Error while processing requester return.';
      return this._err(new Error(msg), xtraData);
    }
    return this._ok(treatedData, xtraData);
  }
  _checkIfFinished() {
    let waitingCount = this._queueData['waitingFor'].length;
    if (waitingCount > 0) {
      this.printDebug('Still waiting for '+ waitingCount +' requests...');
      return;
    }

    let errorCount = this._queueData['errors'].length;
    if (errorCount) {
      this.printDebug(errorCount +' requests with errors. '+
        'Ignoring and using cache for them.',
        'yellow'
      );
    }
    this.printDebug('Checked all requests. :)', 'green');
    this.finished();
  }
  _abort(errMsg, err) {
    this.finished(errMsg, err);
  }


  //After each call of the list
  _empty(xtraData) {
    this.printWarn('Empty return for {{'+ JSON.stringify(xtraData) +'|red}}.');
    this._queueData['errors'].push(xtraData);
    return this._cachedOrAbort(null, xtraData);
  }
  _err(err, xtraData) {
    this.printWarn('Error on request for data: {{'+ JSON.stringify(xtraData) +'|red}}.');
    this._queueData['errors'].push(xtraData);
    return this._cachedOrAbort(err, xtraData);
  }
  _cachedOrAbort(err, xtraData) {
    //FIXME check existance of cache (after implement expiration on cache)
    //      for the error'ed calls before proceding (if no cache, this.abort());
    let cachedData = this.getCachedData(xtraData);
    if (!cachedData) {
      return this._abort(
        'No cached data for '+ this.getID() +', '+ JSON.stringify(xtraData),
        err
      );
    }

    this.printDebug('Found cached data for error\' request '+ JSON.stringify(xtraData) +'.');
    return this._ok(cachedData, xtraData);
  }
  _ok(treatedData, xtraData) {
    this.setCachedData(treatedData, xtraData);
    let key = this.getReturnDataKey(xtraData);
    this._returnData[key] = treatedData;
    this._checkIfFinished();
  }
}

module.exports = BaseCollector;
