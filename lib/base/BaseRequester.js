'use strict';

const BasePrinter = require('./BasePrinter')
;

class BaseRequester extends BasePrinter {
  //Override this
  _doRequest(/*xtraData, cbFunc*/) {
    throw new Error('_doRequest not implemented in child class');
  }

  check(xtraData, cbFunc) {
    if (!cbFunc) { return false; }

    this.printDebug('Update data requested!', 'magenta', xtraData);
    this._doRequest(xtraData, (err, jsonData) => {
      //Error! Damn =/
      if (err) {
        return cbFunc(null, err, xtraData);
      }
      this.printDebug('New data received OK.', 'magenta', xtraData);

      return cbFunc(jsonData, null, xtraData);
    });
    return true;
  }
}

module.exports = BaseRequester;
