'use strict';

const utils = require('simpleutils')
;

class BasePrinter {
  getID() {
    throw new Error('getID not implemented in child class');
  }

  printError(msg, title, xtraData) {
    return utils.print.error('['+ this.getID(xtraData) + '] - '+ msg, title);
  }
  printForce(msg, color, xtraData) {
    return utils.print.force('['+ this.getID(xtraData) + '] - '+ msg, color);
  }
  printWarn(msg, title, xtraData) {
    return utils.print.warn('['+ this.getID(xtraData) + '] - '+ msg, title);
  }
  printNotice(msg, color, xtraData) {
    return utils.print.notice('['+ this.getID(xtraData) + '] - '+ msg, color);
  }
  printInfo(msg, color, xtraData) {
    return utils.print.info('['+ this.getID(xtraData) + '] - '+ msg, color);
  }
  printDebug(msg, color, xtraData) {
    return utils.print.debug('['+ this.getID(xtraData) + '] - '+ msg, color);
  }
  printVerbose(msg, color, xtraData) {
    return utils.print.verbose('['+ this.getID(xtraData) + '] - '+ msg, color);
  }
}

module.exports = BasePrinter;
