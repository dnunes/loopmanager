'use strict';

const BasePrinter = require('./BasePrinter')
;

class BaseStep extends BasePrinter {
  _run() { throw new Error('_run not implemented in child class'); }

  run(sharedStateManager, finishedCB) {
    this._returnData = null;
    this._sharedStateManager = sharedStateManager;
    this._finishedCB = finishedCB;

    this._run();
  }
  finished(errMsg, err) {
    if (errMsg) { this._returnData = null; }
    this._finishedCB(errMsg, this._returnData, err);
  }
}

module.exports = BaseStep;
