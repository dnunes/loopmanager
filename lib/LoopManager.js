'use strict';

const utils = require('simpleutils')
;

class LoopManager {
  constructor(config) {
    if (!config) { config = {}; }

    if (!config.afterCycleInterval) { config.afterCycleInterval = 20; }
    if (!config.afterErrorInterval) { config.afterErrorInterval = 5; }
    if (!config.stepTimeout) { config.stepTimeout = 30; }

    this._cycleCount = 0;
    this._config = config;
    this._steps = [];
    this.curStepIndex = 0;

    this._timer = null;
    this._timeoutTimer = null;

    this._sharedState = {};
    this._sharedStateManager = {
      'get': this.getSharedState.bind(this),
      'set': this.setSharedState.bind(this)
    };
  }

  getCycleCount() { return this._cycleCount; }
  getSharedState(key) {
    return this._sharedState[key] || false;
  }
  setSharedState(key, value) {
    return (this._sharedState[key] = value);
  }

  addStep(func) {
    this._steps.push(func);
  }

  _cleanup() {
    clearTimeout(this._timeoutTimer);
    this._running = false;
    this.curStepIndex = 0;
    this._sharedState = {};
    this._curStepID = null;
  }
  run() {
    if (this._running) {
      utils.print.warn('Already running. Ignoring "run" call.');
      return false;
    }
    this._running = true;
    this._cleanup();

    utils.print.debug('### Starting cycle... ###', 'green');
    setTimeout(this._runStep.bind(this), 0);
    return true;
  }
  _abort(errMsg, err) {
    this._cleanup();
    utils.print.error('ABORTING: '+ errMsg);
    if (err) {
      utils.print.error('Internal error: '+ err);
    }
    this._restart(this._config.afterErrorInterval);
  }
  _finished() {
    this._cycleCount++;
    this._cleanup();
    utils.print.debug('### Finished cycle :) ###', 'green');
    this._restart(this._config.afterCycleInterval);
  }
  _restart(time) {
    if (global.gc) { global.gc(); }
    clearTimeout(this._timer);
    this._timer = setTimeout(this.run.bind(this), time *1000);
  }

  _runStep() {
    let stepInstance = this._steps[this.curStepIndex++];
    if (!stepInstance) {
      return this._finished();
    }
    this._timeoutTimer = setTimeout(
      this._stepTimedout.bind(this), this._config.stepTimeout * 1000
    );
    this._sharedStateManager.set('_curStepID', stepInstance.getID());
    //Trigger the step routine passing in the StateManager and the callback
    stepInstance.run(
      this._sharedStateManager,
      this._stepFinished.bind(this)
    );
    return true;
  }
  _stepTimedout() {
    this._stepFinished(
      'Timedout: exceeded '+ this._config.stepTimeout +' seconds!'
    );
  }
  _stepFinished(errMsg, data, err) {
    clearTimeout(this._timeoutTimer);
    if (errMsg) {
      return this._abort(errMsg, err);
    }

    let stepID = this._sharedStateManager.get('_curStepID');
    if (data) {
      this._sharedStateManager.set('run/'+ stepID, data);
    }
    setTimeout(this._runStep.bind(this), 0);
    return true;
  }
}

module.exports = LoopManager;
