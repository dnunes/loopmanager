LoopManager
===========
### Build _collection+processing_-based apps with ease.

## <a id="installation">Installation</a>
The simplest way to install this package is using [npm](http://www.npmjs.com/):
```bash
$ npm i ??? -S
```

You can also manually download any release from [our GitHub repository](https://github.com/dnunes/loopmanager/) on the [releases page](https://github.com/dnunes/loopmanager/releases/) or try the [latest stable source](https://github.com/dnunes/loopmanager/zipball/master). The links and info are also available on [the project page](http://dnunes.com/loopmanager/).


## <a id="quickstart">Quick Start Guide</a>

There is just X steps needed to start using this package:

1. X
2. X
3. PROFIT!


## <a id="methods">Methods</a>

???


## <a id="advancedusage">Advanced Usage</a>

???


## <a id="credits">Credits</a>

Created and maintained (with much ♡) by [diego nunes](http://dnunes.com)

Donations with Bitcoin to _1PQyeHqusUj3SuTmw6DPqWSHptVHkYZ33R_:

![1PQyeHqusUj3SuTmw6DPqWSHptVHkYZ33R](http://chart.apis.google.com/chart?cht=qr&chs=200x200&chl=bitcoin:1PQyeHqusUj3SuTmw6DPqWSHptVHkYZ33R)
